<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'store' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'FqxFMuDx0Ap9ljencueBLIalI08J4BQujbafBn1Cg3Gi5SO04gJ8kwmhVX1o1soU' );
define( 'SECURE_AUTH_KEY',  'U5qUTXo0uc9KhSbwir32Bz96gGrtMr2ZIkesWmAwPz1fkydPDtyQXhq8CQ8VzsE3' );
define( 'LOGGED_IN_KEY',    'sKk4lXIo2IYNOpIfR6wAsfgUEPMY7Aye3nDPuR6muLGDGAxqFvW5XUJumsDRh7KW' );
define( 'NONCE_KEY',        'z6R2RP7a0I93F7KT0q7wHBSdwOYp1B4QMzSYf6UM4RVjhcqCvkaUVd3V5Bge7dsy' );
define( 'AUTH_SALT',        '1INGmbV3PgefdY7MXOe0qGo65aZDsEeHEC0hXIEoDynH3s62IDRu10eTe66vMy3s' );
define( 'SECURE_AUTH_SALT', 'rlKmRwJVuOjrMQsFKdmM0MUXCw1RTHpzwGqAFlo2deO6NJJqWWy576rSoyVMtTCm' );
define( 'LOGGED_IN_SALT',   'YsIeyO6qjkAqDmwqUQ9wjvboFilPHmBBDbkrzMJ1NYPbOh2bYNc6V8HUikBqdpm0' );
define( 'NONCE_SALT',       'N4KKQV1PCLoMrRFN78TAM6LxehJ1AQMas53W2GM5pWwrzJb2riGnXpa9pZSb9qzA' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
